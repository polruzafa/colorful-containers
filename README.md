# Colorful Containers

## Description

This mod adds several colored shipping containers.
They can be crafted at the Tier 2 Workbench using:

- 1x Shipping Container
- 1x Any Dye Color

## Compatibility

- Compatible with Starbound 1.4
- Compatible with Frackin' Universe

⚠️ There are issues with Improved Containers by v6, but they don't crash the game. ⚠️

## Steam Workshop Links

- Colorful Containers: https://steamcommunity.com/sharedfiles/filedetails/?id=738418305

## Credits

- Me, CC-BY-4.0.